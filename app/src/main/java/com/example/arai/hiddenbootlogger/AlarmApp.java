package com.example.arai.hiddenbootlogger;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

/*
 * Created by arai on 31-08-2017.
 * Simple class to raise periodic alarm to log
 *
 */


public class AlarmApp extends BroadcastReceiver {
    // required to identify and filter various intent received by this service.
    // currently we do not have multiple intent hence safe to ignore.
    public final String START_ALARM = "START_ALARM";
    private final int timeDuration = 1000 * 30 * 1; // Millisec * Second * Minute

    @Override
    public void onReceive(Context context, Intent intent) {
        //check if we received the expected intent broadcast.
        if (START_ALARM.equals(intent.getAction())) {
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");

            // although we do not need to acquire the wake log for this simple logging behaviour, we can chose to comment the below line.
            // however we may need to acquire the wakelog if we have to do long running task such as  downloading GPS tracking etc.
            wl.acquire();

            Log.d("arai", "Amit Test print every 30 second");
            Toast.makeText(context, "I am printing every 30 second!!!", Toast.LENGTH_LONG).show(); // For example
            // should be in same state as acquire
            wl.release();
        }

    }

    public void setAlarm(Context context) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, AlarmApp.class);
        i.setAction(START_ALARM);
        PendingIntent pi = PendingIntent.getBroadcast(context, 1, i, 0);
        // set a repeating alarm to be fired after every 30 seconds - for e.g.
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), timeDuration, pi);
    }

    public void cancelAlarm(Context context) {
        Intent intent = new Intent(context, AlarmApp.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }
}