package com.example.arai.hiddenbootlogger;

import android.content.ComponentName;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        PackageManager p = getPackageManager();

        // activity which is first time open in manifest file which is declare as <category android:name="android.intent.category.LAUNCHER" />
        ComponentName componentName = new ComponentName(this, com.example.arai.hiddenbootlogger.MainActivity.class);
        p.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
        // start the alarm even on first run.
        AlarmApp alapp = new AlarmApp();
        alapp.setAlarm(this);
    }
}
