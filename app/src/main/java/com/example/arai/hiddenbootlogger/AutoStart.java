package com.example.arai.hiddenbootlogger;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by arai on 31-08-2017.
 * simple class to received boot up notification to start alarm service
 */

public class AutoStart extends BroadcastReceiver {
    AlarmApp alarm = new AlarmApp();

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            alarm.setAlarm(context);
        }
    }
}