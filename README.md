## StartOnBootLogger







### Overview
Example Gradle project which demonstrate the functionality of how to setup a timer for e.g 30 seconds and display a toast messages.

### Features, Assumptions and refrences

- This app will hide the icon after first install launch.
- This app will register for Alarm manager to start logging repetodly after every 30 seconds.
- This app also recived boot complete event to start the logging.

- We need to start the app once to hide the icon.

Refrence:

1) https://stackoverflow.com/questions/4459058/alarm-manager-example
2) http://wangjingke.com/2016/09/23/Multiple-ways-to-schedule-repeated-tasks-in-android
  